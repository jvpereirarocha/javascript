/* filter() -> Retorna os valores do array de acordo com o filtro estabelecido */

const idades = [18, 25, 24, 12, 30, 16, 20, 21, 45, 17]
const arr = [25, 24, 21]

const maiority = idades.filter((age) => {
	return age > 21
})

console.log(maiority)




/* find() -> Retorna o primeiro valor encontrado no array de acordo com o critério estabelecido */

const exactAge = idades.find((especificAge) => {
		return especificAge < 18
})

console.log(`A primeira idade menor do que 18 é: ${exactAge} anos`)



/* 
	map() -> Retorna um novo array com o resultado de uma função provida para cada um dos elementos
	No exemplo abaixo, todos os números do array foram divididos por 2
*/

const result = idades.map((element) => {
	return parseFloat(element / 2)
})
console.log('\n')

console.log(result)


/* reduce() -> Reduz o resultado de todos os elementos de um array em uma única saída
	Neste exemplo, soma-se todos os elementos do array 'idades' e a soma total é mostrada
 */

const somaTotal = arr.reduce((element, total) => {
	return element + total;
})

console.log('\n')
console.log('A soma total é: ', somaTotal)




